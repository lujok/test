#+OPTIONS: num:nil h:3
#+AUTHOR: Lu Weiqiang
#+DATE: <2015-12-31>
#+DESCRIPTION: 半農的个人笔记本，包括中医学习笔记、文本整理、学拳记录等，人生有大愿力，而后有大建树。
#+KEYWORDS: 中医, 古籍整理, 太极拳

#+begin_export html
<div class="meta">
  <p>桑下书房：这里记录半農的个人笔记，重点关注中医类、古典类、拳理类等，人生有大愿力，而后有大建树。以十年为单位，积累人生资本。</p>
</div>
#+end_export
